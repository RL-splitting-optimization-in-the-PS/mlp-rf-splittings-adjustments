"""
Provide model for feature extraction.
"""

from pathlib import Path

import mlp_model_api
import numpy as np
import torch
from torchvision import transforms  # , utils

from .feat_extractors.CNN_tri_new import CNN_tri_new
from .feat_extractors.dataloader import ToTensor
from .feat_extractors.vpc_functions import (
    isolate_bunches_from_dm_profile_tri,
    process_tomoscope_acquisition,
)
from .SAC_agents.sac_agents import (
    SacAgentPhase,
    SacAgentQuadP42,
    SacAgentQuadP84,
    SacAgentVolt,
)

__version__ = "0.1.9"


class FeatExtrModelTri(mlp_model_api.MlpModel):

    def __init__(self):
        self.model = CNN_tri_new(trim_edges=True)

    def load_parameters(self, parameters_src: Path) -> None:
        raw_params_src = r"{}".format(parameters_src)
        checkpoint = torch.load(raw_params_src, weights_only=True)
        self.model.load_state_dict(checkpoint["model_state_dict"])

    def export_parameters(self, parameters_target: Path) -> None:
        torch.save(
            {
                "epoch": 0,
                "model_state_dict": self.model.state_dict(),
            },
            parameters_target,
        )

    def predict(self, input_data):
        """Input data already pre-processed to correct format, 185x320."""
        inputs = input_data[mlp_model_api.INPUTS]

        sample_trim = {"image": inputs, "labels": np.ndarray([0])}
        transform = transforms.Compose(
            [
                ToTensor(),
            ]
        )
        inputs_dict_tensor = transform(sample_trim)

        image_tensor = inputs_dict_tensor["image"].unsqueeze(dim=0)
        print(image_tensor.shape)
        outputs = (
            self.model(image_tensor).detach().numpy() / 100
        )  # Convert to numpy and degrees
        outputs[0][2] = (
            outputs[0][2] / 10
        )  # Convert voltage to multiplication factor w.r.t. ref.
        outputs = outputs[0]

        return {mlp_model_api.OUTPUTS: outputs}


class SacPhaseTriModel(mlp_model_api.MlpModel):

    # As input, give fwhms and intensities of the outer two bunches.
    # They are normalized and moved relative to zero inside the model class.

    def __init__(self):
        self.model = SacAgentPhase()

    def load_parameters(self, parameters_src: Path) -> None:
        self.model.agent.set_parameters(parameters_src)

    def export_parameters(self, parameters_target: Path):
        with open(parameters_target, "wb") as outfile:
            self.model.agent.save(outfile)

    def predict(self, input_data):
        inputs = input_data[mlp_model_api.INPUTS]

        phase_obs = inputs

        outputs, _ = self.model.agent.predict(phase_obs, deterministic=True)
        max_step_size = 10
        converted_action = np.array([0.0, 0.0], dtype=object)  # Scale back to degrees
        converted_action[0] = outputs[0] * max_step_size
        converted_action[1] = outputs[1] * max_step_size

        return {mlp_model_api.OUTPUTS: converted_action}


class SacVoltTriModel(mlp_model_api.MlpModel):

    # As input, give relative fwhms and intensities of the all three bunches.

    def __init__(self):
        self.model = SacAgentVolt()

    def load_parameters(self, parameters_src: Path) -> None:
        self.model.agent.set_parameters(parameters_src)

    def export_parameters(self, parameters_target: Path):
        with open(parameters_target, "wb") as outfile:
            self.model.agent.save(outfile)

    def predict(self, input_data):
        inputs = input_data[mlp_model_api.INPUTS]

        volt_obs = inputs

        outputs, _ = self.model.agent.predict(volt_obs, deterministic=True)
        converted_action = np.array([0.0], dtype=object)
        converted_action[0] = outputs[0]

        # How to define output? Any step is defined w.r.t. a  reference voltage program...

        return {
            mlp_model_api.OUTPUTS: converted_action
        }  # Outputs value between -1,1, needs to be handled outside model.


class SacP42QuadModel(mlp_model_api.MlpModel):

    # As input, give fwhms and intensities of the outer two bunches.
    # They are normalized and moved relative to zero inside the model class.

    def __init__(self):
        self.model = SacAgentQuadP42()

    def load_parameters(self, parameters_src: Path) -> None:
        self.model.agent.set_parameters(parameters_src)

    def export_parameters(self, parameters_target: Path):
        with open(parameters_target, "wb") as outfile:
            self.model.agent.save(outfile)

    def predict(self, input_data):
        inputs = input_data[mlp_model_api.INPUTS]

        p42_obs = inputs
        # fwhms = inputs[:3]
        # intensities = inputs[3:]
        # intensities = intensities / np.max(intensities) # Same as in sim.
        # intensities = intensities - np.mean(intensities)
        # fwhms = fwhms / np.max(fwhms)
        # fwhms = fwhms - np.mean(fwhms)
        # volt_obs = np.append(fwhms, intensities)

        outputs, _ = self.model.agent.predict(p42_obs, deterministic=True)
        # max_voltage_step = 0.1
        converted_action = np.array([0.0], dtype=object)
        converted_action[0] = outputs[0] * 20  # Scale back to degrees

        # How to define output? Any step is defined w.r.t. a  reference voltage program...

        return {
            mlp_model_api.OUTPUTS: converted_action
        }  # Outputs value between -1,1, needs to be handled outside model.


class SacP84QuadModel(mlp_model_api.MlpModel):

    # As input, give fwhms and intensities of the outer two bunches.
    # They are normalized and moved relative to zero inside the model class.

    def __init__(self):
        self.model = SacAgentQuadP84()

    def load_parameters(self, parameters_src: Path) -> None:
        self.model.agent.set_parameters(parameters_src)

    def export_parameters(self, parameters_target: Path):
        with open(parameters_target, "wb") as outfile:
            self.model.agent.save(outfile)

    def predict(self, input_data):
        inputs = input_data[mlp_model_api.INPUTS]

        p84_obs = inputs
        # fwhms = inputs[:3]
        # intensities = inputs[3:]
        # intensities = intensities / np.max(intensities) # Same as in sim.
        # intensities = intensities - np.mean(intensities)
        # fwhms = fwhms / np.max(fwhms)
        # fwhms = fwhms - np.mean(fwhms)
        # volt_obs = np.append(fwhms, intensities)

        outputs, _ = self.model.agent.predict(p84_obs, deterministic=True)
        # max_voltage_step = 0.1
        converted_action = np.array([0.0], dtype=object)
        converted_action[0] = outputs[0] * 20  # Scale back to degrees

        # How to define output? Any step is defined w.r.t. a  reference voltage program...

        return {
            mlp_model_api.OUTPUTS: converted_action
        }  # Outputs value between -1,1, needs to be handled outside model.
