"""
High-level tests for the  package.

"""

import mlp_rf_splittings


def test_version():
    assert mlp_rf_splittings.__version__ is not None
