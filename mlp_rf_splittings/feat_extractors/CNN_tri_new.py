import os
from pathlib import Path

import mlp_model_api
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

#from pytorch_fitmodule import FitModule
from torch.autograd import Variable
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms  # , utils

from .dataloader import ToTensor
from .vpc_functions import process_tomoscope_acquisition

os.environ["PATH"] += os.pathsep + 'C:/Program Files/Graphviz/bin/'

class MlpFeatExtractorTriple(mlp_model_api.MlpModel):

    def __init__(self):
        self.model = CNN_tri_new(trim_edges=True)

    def load_parameters(self, parameters_src: Path) -> None:
        raw_params_src = r'{}'.format(parameters_src) # Why is this needed
        checkpoint = torch.load(raw_params_src, weights_only=True)
        self.model.load_state_dict(checkpoint['model_state_dict'])

    def export_parameters(self, parameters_target: Path) -> None:
        torch.save({
                'epoch': 0,
                'model_state_dict': self.model.state_dict(),
                }, parameters_target)

    def predict(self, input_data):
        inputs = input_data[mlp_model_api.INPUTS]

        # Data processing: input is raw numpy array
        values_centered_norm = process_tomoscope_acquisition(inputs, mode='tri', remove_tails=True, correct_dm=True)
        values_centered_norm_trimmed = values_centered_norm[:,40:360]
        sample_trim = {'image': values_centered_norm_trimmed, 'labels': np.ndarray([0])}
        transform = transforms.Compose([
            #Normalize(),
            ToTensor(),
            ])
        inputs_dict_trim = transform(sample_trim)
        image_trim = inputs_dict_trim['image'].unsqueeze(dim=0)
        outputs = self.model(image_trim).detach().numpy()/100 # Convert to numpy and degrees
        outputs[0][2] = outputs[0][2]/10 # Convert voltage to multiplication factor w.r.t. ref.
        outputs = outputs[0]
        #print(f'Feat extractor predicted phase offsets: {outputs[0][:2]}')        
        
        return {mlp_model_api.OUTPUTS: outputs} # Scale back to degrees


def linear_layers(input_feats, num_features):
    dense_layers = nn.Sequential(
            torch.nn.Linear(input_feats,256), # When using original sim sizes
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Linear(256, num_features)
        )
    return dense_layers

class CNN_tri_new(torch.nn.Module):
    def __init__(self, num_features=3, trim_edges=False):
        super(CNN_tri_new, self).__init__()
        

        self.conv_layers = nn.Sequential(# Input is 1x150x400 
            torch.nn.Conv2d(1,64,3,stride=2), # 
            torch.nn.BatchNorm2d(64),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(64,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Flatten(),
            
        )
        if trim_edges:
            flattened_feats = 864
        else:
            flattened_feats = 1056
        self.dense_layers = linear_layers(flattened_feats, num_features)

    def forward(self, x):                                        
        x = x.float()
        
        x = self.conv_layers(x)
        out = self.dense_layers(x)
        return out

class CNN_tri_avg_pool(torch.nn.Module):
    def __init__(self, num_features=3, trim_edges=False):
        super(CNN_tri_avg_pool, self).__init__()
        

        self.conv_layers = nn.Sequential(# Input is 1x150x400
            torch.nn.AvgPool2d(kernel_size=(2,4),stride=2),
            torch.nn.Conv2d(1,64,3,stride=2), #
            torch.nn.BatchNorm2d(64),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(64,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Flatten(),
            
        )
        if trim_edges:
            flattened_feats = 864
        else:
            flattened_feats = 1056
        self.dense_layers = linear_layers(flattened_feats, num_features)

    def forward(self, x):                                        
        x = x.float()
        
        x = self.conv_layers(x)
        out = self.dense_layers(x)
        return out

if __name__ == "__main__":
    cnn = CNN_tri_avg_pool(trim_edges=True)
    nbr_params = count_parameters(cnn)
    make_graph = True
    if make_graph:
        dataset = QuadsplitDataset(noise=True, trim_edges=True, csv_file = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset\tri\ref_voltage\dataset_tri_59521_ref.csv",
        transform=transforms.Compose([
            Normalize(),
            ToTensor(),
        ]))
        dataloader = DataLoader(dataset, batch_size=5,  shuffle=False, num_workers=0)
        data = next(iter(dataloader))
        inputs, labels = data['image'].float(), data['labels'].float()
        test = torch.nn.AvgPool2d(kernel_size=(2,4),stride=2)
        inp = test(inputs) # Check effect of avg pooling: Smoothing
        outputs = cnn(inputs)
        #torchviz.make_dot(outputs, params=dict(list(cnn.named_parameters()))).render("cnn_torchviz", format="png")
    print(cnn)
    print(nbr_params)