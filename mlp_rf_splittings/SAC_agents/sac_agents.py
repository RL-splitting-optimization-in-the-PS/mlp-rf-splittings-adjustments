import gym
import numpy as np
from stable_baselines3 import SAC


class SacAgentPhase:

    def __init__(self):
        self.agent = SAC(
            "MlpPolicy",
            env=SacPhaseEnv(),
            ent_coef=1 / 5,
        )


class SacAgentVolt:

    def __init__(self):
        self.agent = SAC(
            "MlpPolicy",
            env=SacVoltEnv(),
            ent_coef=1 / 5,
        )


class SacAgentQuadP42:

    def __init__(self):
        self.agent = SAC(
            "MlpPolicy",
            env=SacDoubleEnv(),
            ent_coef=1 / 5,
        )


class SacAgentQuadP84:

    def __init__(self):
        self.agent = SAC(
            "MlpPolicy",
            env=SacDoubleEnv(),
            ent_coef=1 / 5,
        )


class SacPhaseEnv(gym.Env):
    """
    Skeleteon environment for the triple split phase optimization.

    Only used to declare action and observation spaces of model.
    """

    ### Domain declaration
    action_space = gym.spaces.Box(
        np.array([-1, -1]), np.array([1, 1]), shape=(2,), dtype=np.float32
    )
    observation_space = gym.spaces.Box(
        np.array(
            [-1, -1, -1, -1]
        ),  # RELATIVE! bl1,bl3, int1, int3, feature extracted p14,p21
        np.array([1, 1, 1, 1]),
        shape=(4,),
        dtype=np.float32,
    )

    def __init__(self):
        """
        Initialize the environment
        """

        pass

        ### Reset
        # self.reset()

    def step(self, action):
        """
        One step/action in the environment, returning the observable
        and reward.

        Stopping conditions: max_steps reached, or small enough difference between bunch lengths reached (BUNCH_LENGTH_CRITERIA)
        """

        return  # state, reward, self.is_finalized, info

    def _take_action(self, action):
        """
        Actual action funtion.

        Action from model is scaled to be between [-1,1] for better optimization performance.
        Converted back to phase setting in degrees using self.max_step_size.
        """

        pass

    def _get_state(self):
        """
        Get the observable for a given phase_set

        Comment: The edge cases handling datapoints outside the simulated data should be investigated more, and perhaps more cases need to be added.
        The important factors considered when making it as it is now was to make sure that all edge cases are covered by some state, and
        that the different edge cases lead to different states (so the model can learn what steps to take in what areas). In this way,
        the RL agent should learn to at least step back in the simulated area when going outside.
        """

        state = np.array([-0.015, -0.05, 0.2, 0.2])

        return state

    def _get_reward(self):
        """
        Evaluating the reward from the observable

        TESTED FUNCTIONS: Gauss, Simple. Others may be deprecated.
        """

        reward = -0.1
        return reward

    def reset(self):
        """
        Reset to a random state to start over the training
        """

        # Resetting to start a new episode
        pass


class SacVoltEnv(gym.Env):
    """
    Skeleteon environment for the triple split voltage optimization.

    Only used to declare action and observation spaces of model.
    """

    ### Domain declaration
    action_space = gym.spaces.Box(
        np.array([-1]), np.array([1]), shape=(1,), dtype=np.float32
    )
    observation_space = gym.spaces.Box(
        np.array([-1, -1, -1, -1, -1, -1]),  # RELATIVE! bl1, bl2, bl3, int1, int2, int3
        np.array([1, 1, 1, 1, 1, 1]),
        shape=(6,),
        dtype=np.float32,
    )

    def __init__(self):
        """
        Initialize the environment
        """

        pass

        ### Reset
        # self.reset()

    def step(self, action):
        """
        One step/action in the environment, returning the observable
        and reward.

        Stopping conditions: max_steps reached, or small enough difference between bunch lengths reached (BUNCH_LENGTH_CRITERIA)
        """

        return  # state, reward, self.is_finalized, info

    def _take_action(self, action):
        """
        Actual action funtion.

        Action from model is scaled to be between [-1,1] for better optimization performance.
        Converted back to phase setting in degrees using self.max_step_size.
        """

        pass

    def _get_state(self):
        """
        Get the observable for a given phase_set

        Comment: The edge cases handling datapoints outside the simulated data should be investigated more, and perhaps more cases need to be added.
        The important factors considered when making it as it is now was to make sure that all edge cases are covered by some state, and
        that the different edge cases lead to different states (so the model can learn what steps to take in what areas). In this way,
        the RL agent should learn to at least step back in the simulated area when going outside.
        """

        state = np.array([-0.015, -0.05, 0.2, 0.2])

        return state

    def _get_reward(self):
        """
        Evaluating the reward from the observable

        TESTED FUNCTIONS: Gauss, Simple. Others may be deprecated.
        """

        reward = -0.1
        return reward

    def reset(self):
        """
        Reset to a random state to start over the training
        """

        # Resetting to start a new episode
        pass


class SacDoubleEnv(gym.Env):
    """
    Skeleton environment for the either double split in the quadsplit optimization.

    Load the corresponding weights to the phases you want to optimize for best performance
    (either p42 or p84).

    Only used to declare action and observation spaces of model.
    """

    ### Domain declaration
    action_space = gym.spaces.Box(
        np.array([-1]), np.array([1]), shape=(1,), dtype=np.float32
    )
    observation_space = gym.spaces.Box(
        np.array(
            [-1, -1, -1, -1]
        ),  # RELATIVE! bl1,bl3, int1, int3, feature extracted p14,p21
        np.array([1, 1, 1, 1]),
        shape=(4,),
        dtype=np.float32,
    )

    def __init__(self):
        """
        Initialize the environment
        """

        pass

        ### Reset
        # self.reset()

    def step(self, action):
        """
        One step/action in the environment, returning the observable
        and reward.

        Stopping conditions: max_steps reached, or small enough difference between bunch lengths reached (BUNCH_LENGTH_CRITERIA)
        """

        return  # state, reward, self.is_finalized, info

    def _take_action(self, action):
        """
        Actual action funtion.

        Action from model is scaled to be between [-1,1] for better optimization performance.
        Converted back to phase setting in degrees using self.max_step_size.
        """

        pass

    def _get_state(self):
        """
        Get the observable for a given phase_set

        Comment: The edge cases handling datapoints outside the simulated data should be investigated more, and perhaps more cases need to be added.
        The important factors considered when making it as it is now was to make sure that all edge cases are covered by some state, and
        that the different edge cases lead to different states (so the model can learn what steps to take in what areas). In this way,
        the RL agent should learn to at least step back in the simulated area when going outside.
        """

        state = np.array([-0.015, -0.05, 0.2, 0.2])

        return state

    def _get_reward(self):
        """
        Evaluating the reward from the observable

        TESTED FUNCTIONS: Gauss, Simple. Others may be deprecated.
        """

        reward = -0.1
        return reward

    def reset(self):
        """
        Reset to a random state to start over the training
        """

        # Resetting to start a new episode
        pass
