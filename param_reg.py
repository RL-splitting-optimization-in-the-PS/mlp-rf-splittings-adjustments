from mlp_client import AUTO, Client, Profile
from mlp_rf_splittings.feat_extractors.CNN_tri_new import MlpFeatExtractorTriple
from pathlib import Path


model = MlpFeatExtractorTriple()
feat_model_name = 'cnn_tri_Trim_Move_Injection_PV_59521_ref_voltageJul_11_2022_09-22-53_e43' # Trim edges version
path = r"/eos/home-j/jwulff/workspaces/mlp-rf-splittings/CNN_models/{}.pth".format(feat_model_name)
model.load_parameters(path)

client = Client(Profile.PRO)
output = client.publish_model_parameters_version(
    model,
    name="FEAT_EXTR_TRIPLE_SPLITTING",
    version=AUTO,
)
print(output)

