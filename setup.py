"""
setup.py for mlp-rf-splittings.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
from pathlib import Path

from setuptools import find_packages, setup

HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt', encoding='utf-8') as fh:
    LONG_DESCRIPTION = fh.read().strip()

REQUIREMENTS: dict = {
    'core': [
        #"cernml.coi ~= 0.8.0", # Stable-Baselines3 included?
        "gym >= 0.21.0",
        "matplotlib ~= 3.0",
        "numpy >= 1.0",
        "scipy >= 1.5",
        "pyjapc >= 2.0",
        "torch ~= 2.5.0",
        "torchvision >= 0.12.0",
        "stable_baselines3 >= 1.5.0",
        "cloudpickle",
        "shimmy[gym-v21]",
        "accwidgets[all-widgets]",
        "mlp-model-api",
    ],
    'test': [
        'pytest',
    ],
    'dev': [
        "cern-mlp-client",  # Required for parameter registrations.
        # 'requirement-for-development-purposes-only',
    ],
    'doc': [
        'sphinx',
        'acc-py-sphinx',
    ],
}


setup(
    name='mlp-rf-splittings',
    version="0.1.9",

    author='Joel Wulff',
    author_email='joel.wulff@cern.ch',
    description='SHORT DESCRIPTION OF PROJECT',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    url='',
    package_data={'':
                    ['mlp-models.toml'],
                  },
    packages=find_packages(),
    python_requires='~=3.11',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],

    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    },
)
