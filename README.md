# mlp-rf-splittings

## Purpose of this project
Implementation of pre-trained reinforcement learning and feature extractors in the Machine Learning Platform (MLP) for use in automated optimization of longitudinal triple/quadruple splittings in the Proton Synchrotron at CERN. 

Developed by Joel Wulff during technical studentship 2022-2023.

## Usage

Currently, this repository is only to be used for deployment of pre-trained agents/CNNs, as the MLP implementations have not been made trainable. However, this could be adapted in the future. Currently, any training of new models could be done using the repository here: https://gitlab.cern.ch/RL-splitting-optimization-in-the-PS/training-ml-models-for-splitting-optimization
